package br.com.joyjet.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.joyjet.dto.JsonData;
import br.com.joyjet.model.Article;
import br.com.joyjet.model.Cart;
import br.com.joyjet.model.Item;
import br.com.joyjet.model.DeliveryFee;
import br.com.joyjet.model.Discount;

public class EcommerceService {
	private Map<Long, Article> mapArticle = new HashMap<Long, Article>();
	private Map<Long, Discount> mapDiscount = new HashMap<Long, Discount>();
	private List<Cart> carts = new ArrayList<Cart>();
	private List<DeliveryFee> deliveries = new ArrayList<DeliveryFee>();

	public EcommerceService(JsonData jsonData) {
		super();
		for (Article article : jsonData.getArticles()) {
			mapArticle.put(article.getId(), article);
		}
		for (Discount discount : jsonData.getDiscounts()) {
			mapDiscount.put(discount.getArticle_id(), discount);
		}
		this.carts = jsonData.getCarts();
		this.deliveries = jsonData.getDelivery_fees();
	}

	public List<Cart> getCarts() {
		return carts;
	}
	
	public List<DeliveryFee> getDeliveries() {
		return deliveries;
	}


	public Integer getPriceById(Long id){
		return mapArticle.get(id).getPrice();
	}
	
	public Discount getDiscountById(Long id){
		return mapDiscount.get(id);
	}
	
	public Integer getArticleValueById(Long id){
		if(mapDiscount.containsKey(id))
			return getValueWithDiscount(id);
		return getPriceById(id);
	}
	
	public Integer getValueWithDiscount(Long id) {
		if(getDiscountById(id).isAmount())
			return (int) (getPriceById(id) - getDiscountById(id).getValue());
		return (int) (getPriceById(id)* ( 1 - getDiscountById(id).getValue()/100));
	}
	
	public void checkoutCarts() {
		for (Cart cart : getCarts()) {
			sumItemValues(cart);
		}
	}
	
	public void sumItemValues(Cart cart) {
		for (Item item : cart.getItems()) {
			cart.incrementTotal(item.getQuantity() * getArticleValueById(item.getArticle_id()));
		}
		addDeliveryValue(cart);
	}
	
	public void addDeliveryValue(Cart cart){
    	for (DeliveryFee fee : getDeliveries()) {
			if (validRange(fee, cart)) {
				cart.incrementTotal(fee.getPrice());
				break;
			}
		}
    }
	
	public boolean validRange(DeliveryFee fee, Cart cart) {
		return (cart.getTotal() >= fee.getEligible_transaction_volume().getMin_price()
				&& fee.getEligible_transaction_volume().getMax_price() == null)
				|| (cart.getTotal() >= fee.getEligible_transaction_volume().getMin_price()
				&& cart.getTotal() < fee.getEligible_transaction_volume().getMax_price());
	}
	
}
