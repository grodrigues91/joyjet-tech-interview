package br.com.joyjet.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import br.com.joyjet.dto.JsonData;
import br.com.joyjet.service.EcommerceService;

public class Ecommerce {

	private static final String PATH = "C:/Users/Gabriel/Desktop/datav3.json";

	public static void main(String[] args) throws IOException {
		JsonData jsonData = readJson();
		
		EcommerceService service = new EcommerceService(jsonData);
		service.checkoutCarts();

		writeJson(service);

	}

	private static void writeJson(EcommerceService service) throws IOException {
		try (Writer writer = new FileWriter("Output.json")) {

			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

			JsonObject jsonObject = new JsonObject();
			jsonObject.add("carts", gson.toJsonTree(service.getCarts()));

			gson.toJson(jsonObject, writer);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static JsonData readJson() throws FileNotFoundException {
		BufferedReader data = new BufferedReader(new FileReader(PATH));
		JsonData jsonData = new Gson().fromJson(data, JsonData.class);
		return jsonData;
	}

}
