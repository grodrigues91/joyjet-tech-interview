package br.com.joyjet.model;

public class DeliveryFee {
	private EligibleTransactionVolume eligible_transaction_volume;
	private Integer price;

	
	public DeliveryFee(EligibleTransactionVolume eligible_transaction_volume,
			Integer price) {
		super();
		this.eligible_transaction_volume = eligible_transaction_volume;
		this.price = price;
	}

	public EligibleTransactionVolume getEligible_transaction_volume() {
		return eligible_transaction_volume;
	}

	public void setEligible_transaction_volume(
			EligibleTransactionVolume eligible_transaction_volume) {
		this.eligible_transaction_volume = eligible_transaction_volume;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((eligible_transaction_volume == null) ? 0
						: eligible_transaction_volume.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryFee other = (DeliveryFee) obj;
		if (eligible_transaction_volume == null) {
			if (other.eligible_transaction_volume != null)
				return false;
		} else if (!eligible_transaction_volume
				.equals(other.eligible_transaction_volume))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}
	
	

}
