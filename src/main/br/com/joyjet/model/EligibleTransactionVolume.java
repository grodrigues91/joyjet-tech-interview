package br.com.joyjet.model;

public class EligibleTransactionVolume {
	
	private Integer min_price;
	private Integer max_price;
	
	public EligibleTransactionVolume(Integer min_price, Integer max_price) {
		super();
		this.min_price = min_price;
		this.max_price = max_price;
	}
	public Integer getMin_price() {
		return min_price;
	}
	public void setMin_price(Integer min_price) {
		this.min_price = min_price;
	}
	public Integer getMax_price() {
		return max_price;
	}
	public void setMax_price(Integer max_price) {
		this.max_price = max_price;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((max_price == null) ? 0 : max_price.hashCode());
		result = prime * result
				+ ((min_price == null) ? 0 : min_price.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EligibleTransactionVolume other = (EligibleTransactionVolume) obj;
		if (max_price == null) {
			if (other.max_price != null)
				return false;
		} else if (!max_price.equals(other.max_price))
			return false;
		if (min_price == null) {
			if (other.min_price != null)
				return false;
		} else if (!min_price.equals(other.min_price))
			return false;
		return true;
	}
	
	
	
	
}
