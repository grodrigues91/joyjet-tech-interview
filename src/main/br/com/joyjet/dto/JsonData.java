package br.com.joyjet.dto;


import java.util.List;

import br.com.joyjet.model.Article;
import br.com.joyjet.model.Cart;
import br.com.joyjet.model.DeliveryFee;
import br.com.joyjet.model.Discount;


public class JsonData {
	
	private List<Article> articles;
	private List<Cart> carts;
	private List<DeliveryFee> delivery_fees;
	private List<Discount> discounts;
	
	public JsonData() {
		super();
	}
	
	public JsonData(List<Article> articles, List<Cart> carts, List<DeliveryFee> delivery_fees,
			List<Discount> discounts) {
		super();
		this.articles = articles;
		this.carts = carts;
		this.delivery_fees = delivery_fees;
		this.discounts = discounts;
	}
	
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	public List<Cart> getCarts() {
		return carts;
	}
	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}
	public List<DeliveryFee> getDelivery_fees() {
		return delivery_fees;
	}
	public void setDelivery_fees(List<DeliveryFee> delivery_fees) {
		this.delivery_fees = delivery_fees;
	}
	public List<Discount> getDiscounts() {
		return discounts;
	}
	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}
	
     
}
