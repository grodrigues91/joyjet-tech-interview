package br.com.joyjet.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.joyjet.dto.JsonData;
import br.com.joyjet.model.Article;
import br.com.joyjet.model.Cart;
import br.com.joyjet.model.Item;
import br.com.joyjet.service.EcommerceService;
import br.com.joyjet.model.DeliveryFee;
import br.com.joyjet.model.EligibleTransactionVolume;
import br.com.joyjet.model.Discount;

public class EcommerceServiceTest {
	
	private EcommerceService service;
	private Cart cart;
	
	@Before
	public void init(){
		service = new EcommerceService(getJsonData());
		cart = new Cart(getItems());
	}
	
	private JsonData getJsonData(){
		return new JsonData(getArticles(), null,getDeliveryFees(),getDiscounts());
	}
	
	private List<Article> getArticles(){
		return Arrays.asList(new Article(1l, "water", 100), new Article(2l, "honey", 200), new Article(3l, "mango", 400),new Article(4l, "tea", 1000) );
	}
	
	private List<Item> getItems(){
		return Arrays.asList(new Item(1l, 6), new Item(2l, 2), new Item(4l, 1));
	}
	
	private List<DeliveryFee> getDeliveryFees(){
		return Arrays.asList(new DeliveryFee(getEligibleTransactionVolumes().get(0),800), new DeliveryFee(getEligibleTransactionVolumes().get(1),400), new DeliveryFee(getEligibleTransactionVolumes().get(2),0));
	}
	
	private List<EligibleTransactionVolume> getEligibleTransactionVolumes(){
		return Arrays.asList(new EligibleTransactionVolume(0, 1000),new EligibleTransactionVolume(1000, 2000),new EligibleTransactionVolume(2000, null));
	}
	
	private List<Discount> getDiscounts(){
		return Arrays.asList(new Discount(1l, "amount", 25d), new Discount(2l, "percentage", 10d), new Discount(3l, "percentage", 30d));
	}
	
   
	@Test
	public void isPossibleGetPriceById() {
		assertEquals(100, service.getPriceById(1L).intValue());
		assertEquals(200, service.getPriceById(2L).intValue());
		assertEquals(400, service.getPriceById(3L).intValue());
	}
	
	@Test
	public void isPossibleGetDiscountById(){
		assertEquals(new Discount(1l, "amount", 25d), service.getDiscountById(1L));
		assertEquals(new Discount(2l, "percentage", 10d), service.getDiscountById(2L));
		assertEquals(new Discount(3l, "percentage", 30d), service.getDiscountById(3L));
	}
	
	@Test
	public void isPossibleGetArticleValueById(){
		assertEquals(75, service.getArticleValueById(1l).intValue());
		assertEquals(180, service.getArticleValueById(2l).intValue());
		assertEquals(1000, service.getArticleValueById(4l).intValue());
	}
	
	@Test
	public void isPossibleGetValueWithDiscount(){
		assertEquals(75, service.getValueWithDiscount(1l).intValue());
		assertEquals(180, service.getValueWithDiscount(2l).intValue());
		assertEquals(280, service.getValueWithDiscount(3l).intValue());
	} 
	
	@Test
	public void isPossibleSumItemValues() {
		service.sumItemValues(cart);
		assertEquals(2210,cart.getTotal().intValue());
		
	}
	
	@Test
	public void isPossiblevalidRange(){
		cart.setTotal(500);
		assertEquals(true, service.validRange(getDeliveryFees().get(0),cart));
		assertEquals(false, service.validRange(getDeliveryFees().get(1), cart));
		assertEquals(false, service.validRange(getDeliveryFees().get(2), cart));
	}

}
