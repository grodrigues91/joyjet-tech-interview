package br.com.joyjet.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.joyjet.model.Cart;

public class CartTest {
	
	private Cart cart;
	
	@Before
	public void init(){
		cart = new Cart();
	}

	@Test
	public void isPossibleIncrementTotal() {
	   cart.incrementTotal(100);
	   assertEquals(100, cart.getTotal().intValue());
	   cart.incrementTotal(300);
	   assertEquals(400, cart.getTotal().intValue());
	}

}
